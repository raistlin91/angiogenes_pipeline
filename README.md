# README #

This is the workflow for the ANGIOGENES database

### How do I get set up? ###

* Set up the environment with conda
* Set up the config file
* Download those fastq files, which are not available in the SRA
* Start the workflow
* Be happy


##Setting up the environment with conda
You can install your personal environment without changing your systems and keeping the dependencies required for this pipeline.
If you don't want to set up this environment be sure to install all programs in the requirements.txt file

* install conda
    * go to http://conda.pydata.org/docs/install/quick.html and follow the instructions
    * add the bioconda channel: `conda config --add channels bioconda`
* set up your environment
    * `conda create --name <myenv> --file requirements.txt`
    * `source activate <myenv>` (this has to be done in each new terminal)
* change tophat using python 2
    * because tophat runs only on python 2 at the moment you have to change the shebang
    * open the tophat file (should be under ~/miniconda3/envs/<myenv>/bin/tophat)
    * change the shebang (#!) to `#!/usr/bin/env python2`
## The config file
In the config file you declare which fastq files you want to analyze from the Sequence Read Archive (SRA).
The config file is written in YAML (yaml.org).

For each organism you have to make an array and if it contains single end or paired end reads.
`"<single/paired>_<organism>"`
### example
```
---
"single_zebrafish":
  - SRR1793799
  - SRR1793800
  - SRR1793801
  - SRR1793802
```

Then you have to define the link to the fasta file and to the GTF file:
`"<organism>_ref"` and `"<organism>_gtf"` respectivaly

### example
```
"zebrafish_gtf": "ftp://ftp.ensembl.org/pub/release-84/gtf/danio_rerio/Danio_rerio.GRCz10.84.gtf.gz"
"zebrafish_ref": "ftp://ftp.ensembl.org/pub/release-84/fasta/danio_rerio/dna/Danio_rerio.GRCz10.dna.toplevel.fa.gz"
```
## download unavailable fastq files

If you have fastq files which are not included in the SRA, you can define the link to download the sra file in the config file with:
```
"<single/paired>_<organism>_link":
  "<SampleName>": <link>
```

### example
```
"single_human_link":
  "SRR1640087": ftp://ftp.ddbj.nig.ac.jp/ddbj_database/dra/sralite/ByExp/litesra/SRX/SRX750/SRX750958/SRR1640087/SRR1640087.sra
```

Then you have to start the download process for these files:

`snakemake -s download_snakefile -j 16`
## Start the workflow

`snakemake -j 99 -k`

### How to report an error? ###
send me a mail:
raphael.mueller@bioinfsys.uni-giessen.de

or create an issue
