configfile: "config.yaml"

include: "single_end_rules"
include: "paired_end_rules"


def get_fasta(wild):
	return config[wild+"_ref"]

def get_gtf(wild):
	return config[wild+"_gtf"]


rule all:
	input:
		expand("zebrafish/single/{sample}/cufflinks_out/transcripts.gtf", sample=config['single_zebrafish']),
		expand("mouse/single/{sample}/cufflinks_out/transcripts.gtf", sample=config['single_mouse']),
		expand("human/single/{sample}/cufflinks_out/transcripts.gtf", sample=config['single_human']),
		expand("mouse/paired/{sample}/cufflinks_out/transcripts.gtf", sample=config['paired_mouse']),
		expand("human/paired/{sample}/cufflinks_out/transcripts.gtf", sample=config['paired_human'])



rule bowtie_build:
	input:
		"{organism}/ref/{organism}.fa"
	output:
		"{organism}/ref/{organism}.1.bt2l",
		"{organism}/ref/{organism}.2.bt2l",
		"{organism}/ref/{organism}.3.bt2l",
		"{organism}/ref/{organism}.4.bt2l",
		"{organism}/ref/{organism}.rev.1.bt2l",
		"{organism}/ref/{organism}.rev.2.bt2l"
	shell:
		"bowtie2-build --large-index {input} {wildcards.organism}/ref/{wildcards.organism}"



rule get_ENSEMBL_fasta:
	output:
		"{organism}/ref/{organism}.fa.gz"
	params:
		fa = lambda wildcards: get_fasta(wildcards.organism)
	shell:
		"wget -O {output} {params.fa}"		


rule get_ENSEMBL_annotation:
	output:
		"{organism}/ref/{organism}.gtf.gz"
	params:
		gtf = lambda wildcards: get_gtf(wildcards.organism)
	shell:
		"wget -O {output} {params.gtf}"		

rule gtf_gunzip:
	input:
		"{organism}/ref/{name}.gtf.gz"
	output:
		temp("{organism}/ref/{name}.gtf")
	shell:
		"gunzip -c {input} > {output} || :"

rule fasta_gunzip:
	input:
		"{organism}/ref/{name}.fa.gz"
	output:
		temp("{organism}/ref/{name}.fa")
	shell:
		"gunzip -c {input} > {output} || :"

